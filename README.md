
# Roundglass Backend
This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for?
This repository conatains the code for notification management with following technology stack.
- Express JS as NodeJS framwork
- MongoDB for data storage
- Mongoose for DB Schema creation
- Joi for validation 

## Prerequisite
- Node JS
- MongoDB

## Features Included 
- User Login
- Notification Management

## Instruction to strat development server

  -  Clone the repo and install all the dependencies 
  -  Run `npm start` This will run start node server on http://localhost:8080
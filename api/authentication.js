import Jwt from 'jsonwebtoken';
import Config from '../configrations';
import { User } from './models';
import Utils from './utils';
import appConstants from '../appConstants';
import Boom from 'boom';

const jwtSecret = Config.appConfig.get('/jwtConfig/SECRET_KEY', { env: process.env.NODE_ENV });

module.exports = {
    authMiddleware(req, res, next) {
        const token = req.headers['x-access-token'] || req.headers['token'] || req.query['x-access-token'] || req.headers['Authorization']
        if (token) {
            Jwt.verify(token, jwtSecret, async (err, decoded) => {
                try {
                    if (err) {
                        throw Boom.unauthorized(Utils.responseMessages.get('/INVALID_TOKEN', {
                            contentLanguage: appConstants.DEFAULT_LANGUAGE
                        }))
                    } else {
                        let userInfo = await Utils.queries.findOne(User, { _id: decoded.id });

                        if (!userInfo) {
                            throw Boom.unauthorized(Utils.responseMessages.get('/INVALID_TOKEN', {
                                contentLanguage: appConstants.DEFAULT_LANGUAGE
                            }))
                        }

                        userInfo = userInfo.toJSON()
                        delete userInfo.password;
                        req.user = userInfo;;
                        next()
                    }
                } catch (error) {
                    return Utils.universalFunctions.sendError(error, res)
                }
            });
        } else {
            return Utils.universalFunctions.sendError(Boom.forbidden(Utils.responseMessages.get('/TOKEN_NOT_PROVIDED', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            })), res)
        }
    },
    createToken(payloadData, time) {
        return new Promise((resolve, reject) => {
            Jwt.sign(payloadData, jwtSecret, {
                expiresIn: time
            }, (err, jwt) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(jwt)
                }
            })
        })
    },
}
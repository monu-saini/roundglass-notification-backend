import user from './userController';
import notifications from './notificationController';

module.exports = {
    user,
    notifications
}
import Joi from 'joi';
import Utils, { universalFunctions, queries } from '../utils';
import { UserNotification, Notifications } from '../models';
import Config from '../../configrations';
import appConstants from '../../appConstants';
import Handlebars from 'handlebars';

module.exports = {

  async getUserNotification(req, res) {
    try  {
        const schema = Joi.object().keys({
            skip: Joi.number().optional(),
            limit: Joi.number().optional(),
            language: Joi.string().allow([
                appConstants.CONTENT_LANGUAGE.ENGLISH,
                appConstants.CONTENT_LANGUAGE.RUSSIAN
            ]) 
        });
        
        await universalFunctions.validateRequestPayload(req.query, res, schema);

        let { skip , limit, language} = req.query,
            user = req.user;

        let notifications = await queries.populateAll(UserNotification, { userId: req.user._id}, {}, {
            limit: limit || appConstants.DEFAULT_LIMIT,
            skip: skip || 0
        }, 'notifictionId');

        notifications = notifications.map((obj) => {
            
            let textToSend = obj.notifictionId[language || req.user.contentLanguage || 'default'];
            if(obj.substitution && obj.substitution instanceof Object && Object.keys(obj.substitution).length > 0) {
                let template = Handlebars.compile(textToSend);
                textToSend = template(obj.substitution)
            }
            return {
                notificationText: textToSend,
                type: obj.notifictionId.type
            }
        })

        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: Utils.responseMessages.get('/SUCCESS', {
                contentLanguage: user.contentLanguage
            }),
            data: {
                notifications
            }
        }, res);
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  },

  async addNotification(req, res) {
    try  {
        const schema = Joi.object().keys({
            type: Joi.number().allow(
                appConstants.NOTIFICATION_TYPE.USER_ACTIVITY_NOTIFICATION,
                appConstants.NOTIFICATION_TYPE.SYSTEM_ANNOUNCEMENT,
                appConstants.NOTIFICATION_TYPE.SYSTEM_NOTIFICATION,
            ).required(),
            en: Joi.string().required(),
            ru: Joi.string().required(),
            default: Joi.string().required(),
            identityString: Joi.string().optional()
        });
        
        await universalFunctions.validateRequestPayload(req.body, res, schema);

        await queries.saveData(Notifications, req.body);

        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: Utils.responseMessages.get('/NOTIFICATION_ADDED_SUCCESSFULLY', {
                contentLanguage: req.user.contentLanguage
            }),
            data: { }
        }, res);
        
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  },

  async getNotifications(req, res) {
    try  {
        const schema = Joi.object().keys({
            skip: Joi.number().optional(),
            limit: Joi.number().optional(),
        });
        
        await universalFunctions.validateRequestPayload(req.query, res, schema);

        let { skip , limit } = req.query;

        let query = { isDeleted: false},
            projection = {isDeleted: 0, createdAt: 0, updatedAt: 0, __v: 0},
            options = {
                limit : limit ? parseInt(limit) : appConstants.DEFAULT_LIMIT, 
                skip: skip ? parseInt(skip) : 0
            };


        let notifications = await queries.getData(Notifications, query , projection, options )

        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: Utils.responseMessages.get('/NOTIFICATION_ADDED_SUCCESSFULLY', {
                contentLanguage: req.user.contentLanguage
            }),
            data: { 
                notifications
            }
        }, res);
        
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  },

  async updateNotification(req, res) {
    try  {
        const schema = Joi.object().keys({
            type: Joi.number().allow(
                appConstants.NOTIFICATION_TYPE.USER_ACTIVITY_NOTIFICATION,
                appConstants.NOTIFICATION_TYPE.SYSTEM_ANNOUNCEMENT,
                appConstants.NOTIFICATION_TYPE.SYSTEM_NOTIFICATION,
            ).required(),
            en: Joi.string().required(),
            ru: Joi.string().required(),
            default: Joi.string().required(),
            identityString: Joi.string().optional()
        });
        
        await universalFunctions.validateRequestPayload(req.body, res, schema);

        let updates = {};

        if(req.body.type) updates.type = req.body.type;
        if(req.body.en) updates.en = req.body.en;
        if(req.body.ru) updates.ru = req.body.ru;
        if(req.body.default) updates.default = req.body.default;
        if(req.body.identityString) updates.identityString = req.body.identityString;

        let updatedNotifications = await queries.findAndUpdate(Notifications, {_id: req.params.id} , updates, {new: true})

        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: Utils.responseMessages.get('/NOTIFICATION_UPDATED_SUCCESSFULLY', {
                contentLanguage: req.user.contentLanguage
            }),
            data: updatedNotifications
        }, res);
        
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  },

  async deleteNotification(req, res) {
    try  {
        const schema = Joi.object().keys({
            id: Joi.string().min(24).max(24).required()
        });
        
        await universalFunctions.validateRequestPayload(req.params, res, schema);

        // Use this query to delete parmaenly from DB
        // await queries.remove(Notifications, {_id: req.params.id})
        // Use this query to boolean delete
        await queries.findAndUpdate(Notifications, {_id: req.params.id} , { isDeleted: true})
        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: Utils.responseMessages.get('/NOTIFICATION_REMOVED_SUCCESSFULLY', {
                contentLanguage: req.user.contentLanguage
            }),
            data: {}
        }, res);
        
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  }
};

import Joi from 'joi';
import Utils, { universalFunctions, heplers, queries } from '../utils';
import Boom from 'boom';
import { User } from '../models';
import { createToken } from '../authentication';
import Config from '../../configrations';
import appConstants from '../../appConstants';

module.exports = {

  async auth(req, res) {
    try  {
        const schema = Joi.object().keys({
            email: Joi.string().trim().email().required(),
            password: Joi.string().required()
        });
        
        await universalFunctions.validateRequestPayload(req.body, res, schema);

        let payload = req.body;

        let user = await queries.findOne(User, {
            email: payload.email
        });

        if (!user) {
            throw Boom.badRequest(Utils.responseMessages.get('/USER_NOT_FOUND', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            }));
        }

        let encryptPassword = heplers.encrypt(payload.password);
        
        if (user.get('password') !== encryptPassword) {
            throw Boom.badRequest(Utils.responseMessages.get('/INVALID_CREDENTIALS', {
                contentLanguage: user.contentLanguage
            }));
        }

        user = JSON.parse(JSON.stringify(user));

        delete user.password;
        delete user.createdAt;
        delete user.createdAt;
        delete user.__v;
        
        let expireTime = Config.appConfig.get('/jwtConfig/EXPIRE', {env:process.env.NODE_ENV});

        let token = await createToken({
            id: user._id
        }, expireTime);

        return universalFunctions.sendSuccess({
            statusCode: 200,
            message: Utils.responseMessages.get('/LOGIN_SUCCESSFULLY', {
                contentLanguage: user.contentLanguage
            }),
            data: {
                token,
                user
            }
        }, res);
    } catch(error) {
        return universalFunctions.sendError(error, res);
    }
  },

  logout(req, res) {
    // TODO Remove session from DB but in this demo we are not storing session in db
    let user = req.user;

    return universalFunctions.sendSuccess({
        statusCode: 200,
        message:  Utils.responseMessages.get('/LOGOUT_SUCCESS', {
            contentLanguage: user.contentLanguage
        }),
        data: {}
    }, res);
  }
};

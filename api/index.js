import { user , notifications} from './controllers';
import { authMiddleware } from './authentication';

module.exports = app => {
  //Login API
  app.post('/api/login', user.auth);
  app.post('/api/logout', authMiddleware, user.logout);
  app.get('/api/getNotifications', authMiddleware, notifications.getUserNotification);

  //CURD ON Notifications
  app.post('/api/notification', authMiddleware, notifications.addNotification); //TODO add authorisation
  app.get('/api/notification', authMiddleware, notifications.getNotifications); //TODO add authorisation
  app.patch('/api/notification/:id', authMiddleware, notifications.updateNotification); //TODO add authorisation
  app.delete('/api/notification/:id', authMiddleware, notifications.deleteNotification); //TODO add authorisation
};

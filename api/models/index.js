import User from './users';
import Notifications from './notifications';
import UserNotification from './userNotifications';

module.exports = {
    User,
    Notifications,
    UserNotification
}
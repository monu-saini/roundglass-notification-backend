import Mongoose from 'mongoose';
import APP_CONSTANTS from '../../appConstants';

const notificationSchema = new Mongoose.Schema({
    type: {
        type: Number, 
        enum: [
            APP_CONSTANTS.NOTIFICATION_TYPE.USER_ACTIVITY_NOTIFICATION,
            APP_CONSTANTS.NOTIFICATION_TYPE.SYSTEM_NOTIFICATION,
            APP_CONSTANTS.NOTIFICATION_TYPE.SYSTEM_ANNOUNCEMENT
        ]
    },
    [APP_CONSTANTS.CONTENT_LANGUAGE.ENGLISH]: {
        type: String
    },
    [APP_CONSTANTS.CONTENT_LANGUAGE.RUSSIAN]: {
        type: String
    },
    default: {
        type: String
    },
    identityString: { // We can use this fields to find the Notification Which we want to send on particular event 
        type: String
    },
    isDeleted : {
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = Mongoose.model('notification', notificationSchema)

import Mongoose from 'mongoose';

const userNotificationSchema = new Mongoose.Schema({
    userId: {
        type: Mongoose.Schema.ObjectId,
        ref: 'user'
    },
    notifictionId: {
        type: Mongoose.Schema.ObjectId,
        ref: 'notification'
    },
    substitution: {
        type: Object
    },
}, { timestamps: true })

module.exports = Mongoose.model('userNotification', userNotificationSchema)

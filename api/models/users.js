import Mongoose from 'mongoose';
import APP_CONSTANTS from '../../appConstants';

const userSchema = new Mongoose.Schema({
    email: { type: String, trim: true, index: true, required: true, unique: true },
    password: { type: String },
    name: { type: String, default: "" },
    contentLanguage: {
        type: String, 
        enum: [
            APP_CONSTANTS.CONTENT_LANGUAGE.ENGLISH,
            APP_CONSTANTS.CONTENT_LANGUAGE.RUSSIAN
        ], default: APP_CONSTANTS.CONTENT_LANGUAGE.ENGLISH
    }
}, { timestamps: true })

module.exports = Mongoose.model('user', userSchema)

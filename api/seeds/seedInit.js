import Models from '../models';
import { queries, heplers } from '../utils';
import appConstants from '../../appConstants';
import users from './users.json';
import notifications from './notification.json';

exports.seedInit = async() => {
    try {
        let userData = await queries.findOne(Models.User, {}),
            records = [];
        if(!userData) {
            users.forEach(user => {
                user.password = heplers.encrypt(user.password);
                user.contentLanguage = appConstants.CONTENT_LANGUAGE.ENGLISH;
                records.push(queries.saveData(Models.User, user))
            });
            let addedUsers = await Promise.all(records);
            // Reinitialize variable for secound run 
            records = []
            notifications.forEach(notification => {
                records.push(queries.saveData(Models.Notifications, notification))
            });

            let addedNotifications = await Promise.all(records);
            
            records = [];

            addedNotifications.forEach((notObj) => {
                records.push(queries.saveData(Models.UserNotification, {
                    userId: addedUsers[0].id,
                    notifictionId: notObj.id,
                    substitution: notObj.identityString == "USER_INVITATION" ? {userName: 'Monu', companyName: 'RoundGlass' } : null
                }))
            })

            await Promise.all(records);

            console.log('Fixture data completed');
        }
    } catch (error) {
        console.log('<<<<<<<ERROR IN ADDING ADMINS INTO DB>>>>>>>>', error)
    }
}
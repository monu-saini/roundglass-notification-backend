import Crypto from 'crypto';
import Config from '../../configrations';

function encrypt(text) {
    let algorithm = Config.appConfig.get('/cryptoConfig/ALGORITHM',{env:process.env.NODE_ENV}),
        cryptoKey = Config.appConfig.get('/cryptoConfig/KEY',{env:process.env.NODE_ENV})
    let cipher = Crypto.createCipher(algorithm, cryptoKey)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    let algorithm = Config.appConfig.get('/cryptoConfig/ALGORITHM',{env:process.env.NODE_ENV}),
        cryptoKey = Config.appConfig.get('/cryptoConfig/KEY',{env:process.env.NODE_ENV})
    let decipher = Crypto.createDecipher(algorithm, cryptoKey)
    let dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return decrypted;
}

module.exports = { 
    encrypt,
    decrypt
}
const saveData = (model, data) => {
    return model.create(data);
}

const getData = (model, query, projection, options) => {
    return model.find(query, projection, options);
};

const findOne = (model, query, projection, options) => {
    return model.findOne(query, projection, options);
};

const findAndUpdate = (model, conditions, update, options) => {
    return model.findOneAndUpdate(conditions, update, options)
};

const findOneAndRemove = (model, condition, options) => {
    return model.findOneAndRemove(condition, options)
};

const remove = function (model, condition) {
    return model.remove(condition);
};

const update = (model, conditions, update, options) => {
    return model.update(conditions, update, options)
};

const populateAll = function (model, query, projection, options, collectionOptions) {
    return model.find(query, projection, options).populate(collectionOptions).exec();
};

module.exports = {
    saveData,
    getData,
    update,
    remove,
    findOne,
    findAndUpdate,
    findOneAndRemove,
    populateAll
}
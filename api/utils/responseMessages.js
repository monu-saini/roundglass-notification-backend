const Confidence = require('confidence')
const Store = new Confidence.Store()

const MESSAGES = {
    $filter: 'contentLanguage',
    en: {
        SUCCESS:'Success',
        DB_ERROR: 'Something went wrong.',
        DUPLICATE: 'Duplicate entry.',
        EMAIL_ALREADY_EXISTS:'This user is already registered with us',
        APP_ERROR: 'Application error occurred.',
        INVALID_OBJECT_ID: 'Invalid ID provided.',
        DEFAULT: 'Something went wrong.',
        INVALID_CREDENTIALS: "Incorrect credentials.",
        FAILED: "Failed",
        USER_NOT_FOUND: 'User does not exist',
        LOGIN_SUCCESSFULLY: "You are successfully login.",
        LOGOUT_SUCCESS: "You are successfully logged out",
        UNAUTHORIZED: 'not authenticated',
        NOT_FOUND: "Not Found.",
        INVALID_TOKEN: "Invalid token",
        TOKEN_NOT_PROVIDED: "Token not provided.",
        NOTIFICATION_ADDED_SUCCESSFULLY: "Notification has been added successfully.",
        NOTIFICATION_UPDATED_SUCCESSFULLY: "Notification has been updated successfully.",
        NOTIFICATION_REMOVED_SUCCESSFULLY: "Notification has been removed successfully."
    },
    ru: {
        SUCCESS:'успех',
        DB_ERROR: 'Что-то пошло не так.',
        DUPLICATE: 'Двойная запись',
        EMAIL_ALREADY_EXISTS:'Этот пользователь уже зарегистрирован у нас',
        APP_ERROR: 'Произошла ошибка приложения.',
        INVALID_OBJECT_ID: 'Указан неверный идентификатор.',
        DEFAULT: 'Что-то пошло не так.',
        INVALID_CREDENTIALS: "Неверные учетные данные",
        FAILED: "Не удалось",
        USER_NOT_FOUND: 'Пользователь не существует',
        LOGIN_SUCCESSFULLY: "Вы успешно вошли в систему.",
        LOGOUT_SUCCESS: "Вы успешно вышли из системы",
        UNAUTHORIZED: 'не аутентифицирован',
        NOT_FOUND: "Не обнаружена.",
        INVALID_TOKEN: "Invalid token",
        TOKEN_NOT_PROVIDED: "Token not provided.",
        NOTIFICATION_ADDED_SUCCESSFULLY: "Уведомление было успешно добавлено.",
        NOTIFICATION_UPDATED_SUCCESSFULLY: "Уведомление было успешно обновлено.",
        NOTIFICATION_REMOVED_SUCCESSFULLY: "Уведомление было успешно удалено."
    },
    $default: {
        SUCCESS:'Success',
        DB_ERROR: 'Something went wrong.',
        DUPLICATE: 'Duplicate entry.',
        EMAIL_ALREADY_EXISTS:'This user is already registered with us',
        APP_ERROR: 'Application error occurred.',
        INVALID_OBJECT_ID: 'Invalid ID provided.',
        DEFAULT: 'Something went wrong.',
        INVALID_CREDENTIALS: "Incorrect credentials.",
        FAILED: "Failed",
        USER_NOT_FOUND: 'User does not exist',
        LOGIN_SUCCESSFULLY: "You are successfully login.",
        LOGOUT_SUCCESS: "You are successfully logged out",
        UNAUTHORIZED: 'not authenticated',
        NOT_FOUND: "Not Found.",
        INVALID_TOKEN: "Invalid token",
        TOKEN_NOT_PROVIDED: "Token not provided.",
        NOTIFICATION_ADDED_SUCCESSFULLY: "Notification has been added successfully.",
        NOTIFICATION_UPDATED_SUCCESSFULLY: "Notification has been updated successfully.",
        NOTIFICATION_REMOVED_SUCCESSFULLY: "Notification has been removed successfully."
    }
}

Store.load(MESSAGES)

const get = (key, criteria) => {
    if (criteria) {
        return Store.get(key, criteria)

    }
    else {
        return Store.get(key)
    }
}

module.exports = {
    get
}
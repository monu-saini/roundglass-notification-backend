import Boom from 'boom';
import Joi from 'joi';
import responseMessages from './responseMessages';
import appConstants from '../../appConstants';

const validateRequestPayload = async (requestObj, res, schema) => {
    return new Promise((resolve, reject) => {
        const {
            error
        } = Joi.validate(requestObj, schema)
    
        if (error) {
            return sendBadRequestError(error, res)
        } else {
            resolve()
        }
    })
}

const validateGraphQLPayload = (requestObj, schema) => {
    const { error } = Joi.validate(requestObj, schema);
    if(error) {
        let message = error.details[0].message
        message = message.replace(/"/g, '');
        message = message.replace('[', '');
        message = message.replace(']', '');
        return message
    }
    return false
}

const sendError = (data, res) => {
    let error;
    console.log('ERROR OCCURRED IN SEND ERROR FUNCTION', data)
    let message = null;
    if (typeof data == 'object' && !data.isBoom) {
        if (data.name == 'MongoError') {
            message = responseMessages.get('/DB_ERROR', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            });;
            if (data.code = 11000) {
                if (data.message.split(" ").indexOf('email_1') > -1) {
                    const conflictError = Boom.conflict(responseMessages.get('/EMAIL_ALREADY_EXISTS', {
                        contentLanguage: appConstants.DEFAULT_LANGUAGE
                    }))
                    return res.json(conflictError.output.payload);
                } else {
                    message = responseMessages.get('/DUPLICATE', {
                        contentLanguage: appConstants.DEFAULT_LANGUAGE
                    });
                }
            }
        } else if (data.name == 'ApplicationError') {
            message = responseMessages.get('/APP_ERROR', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            });
        } else if (data.name == 'ValidationError') {
            message = responseMessages.get('/APP_ERROR', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            });
        } else if (data.name == 'CastError') {
            message = responseMessages.get('/INVALID_OBJECT_ID', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            });
        } else if (data.response) {
            message = data.response.message;
        } else if (data.message) {
            message = data.message;
        } else {
            message = responseMessages.get('/DEFAULT', {
                contentLanguage: appConstants.DEFAULT_LANGUAGE
            });
        }
        if (message !== null) {
            error = new Boom(message, {
                statusCode: 400
            })
            return res.json(error.output.payload);
        }

    } else if (typeof data == 'object' && data.isBoom) {
        return res.json(data.output.payload);
    } else {
        error = new Boom(data, {
            statusCode: 400
        })
        return res.json(error.output.payload);
    }

};

/*-------------------------------------------------------------------------------
 * send success
 * -----------------------------------------------------------------------------*/

const sendSuccess = (response, res) => {
    const statusCode = (response && response.statusCode) ? response.statusCode : 200
    const message = (response && response.message) ? response.message : 'Success'
    const data = (response.data) ? response.data : {}
    if (data.password) {
        delete data.password
    }
    return res.json({
        statusCode,
        message,
        data
    })
};

/*-------------------------------------------------------------------------------
 * Joi error handle
 * -----------------------------------------------------------------------------*/
const sendBadRequestError = (error, res) => {
    let message = error.details[0].message
    message = message.replace(/"/g, '');
    message = message.replace('[', '');
    message = message.replace(']', '');
    const errorToSend = Boom.badRequest(message)
    res.send(errorToSend.output.payload)
}

module.exports = {
    validateRequestPayload,
    sendSuccess,
    sendError,
    validateGraphQLPayload
}
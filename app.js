import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import setupApi from './api';
import Config from './configrations';
import bootstrap from './bootstrap';

const dev = process.env.NODE_ENV !== 'production';

const port = Config.appConfig.get('/port/PORT', {
    env: process.env.NODE_ENV
});

const server = express();

if (!dev) {
    server.use(compression());
}

server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
    next();
});

server.use(express.static('public'));
server.use(cookieParser());
server.use(morgan('dev'));
server.use(bodyParser.json());
server.use(
    bodyParser.urlencoded({
        extended: false,
    })
);

setupApi(server);

bootstrap.connectMongo();

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', bootstrap.gracefulExit).on('SIGTERM', bootstrap.gracefulExit);    

server.listen(port, err => {
    if (err) {
        throw err;
    }
    console.log(`Running on localhost:${port}`);
});

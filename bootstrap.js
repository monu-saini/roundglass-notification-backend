
import Mongoose from 'mongoose';
import Config from './configrations';
import { seedInit } from './api/seeds/seedInit';

const databaseUrl = Config.appConfig.get('/mongo/CONNECTING_STRING', {
    env: process.env.NODE_ENV
});

module.exports = {
    connectMongo: function () {

        console.log('Database URL: ', databaseUrl);

        Mongoose.connect(databaseUrl, {
            autoReconnect: true,
            useNewUrlParser: true
        }).then(( () => {
            Mongoose.set('debug', true)
            console.log('Database connected successfully.');
            
        })).catch(err => {
            console.log('[Error connecting to database]', err)
        })

    },
    gracefulExit: function () {
        Mongoose.connection.close(function () {
            console.log('Mongoose default connection with DB :' + databaseUrl + ' is disconnected through app termination');
            process.exit(0);
        });
    }
};

Mongoose
    .connection
    .on('connected', async () => {
        seedInit();
    })



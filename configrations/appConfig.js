import Confidence from 'confidence';

const Store = new Confidence.Store();

const APP_CONFIG = {
    $filter: "env",
    development: {
        cryptoConfig: {
            ALGORITHM: "aes-256-ctr",
            KEY: "wbedibeiwhduwiehdwuihd;ixhewow;do034i0348-0348-034vyiuGVvbgvgv89873%$%#"
        },
        port:{
            PORT:8080
        },
        jwtConfig: {
            SECRET_KEY: "roundglass_123QWERT_KEY",
            EXPIRE: "10 days"
        },
        mongo: {
            CONNECTING_STRING: "mongodb://127.0.0.1/roundglass"
        }
    },
    production: {
        cryptoConfig: {
            ALGORITHM: "aes-256-ctr",
            KEY: "wbedibeiwhduwiehdwuihd;ixhewow;do034i0348-0348-034vyiuGVvbgvgv89873%$%#"
        },
        port:{
            PORT:8080
        },
        jwtConfig: {
            SECRET_KEY: "roundglass_123QWERT_KEY",
            EXPIRE: "10 days"
        },
        mongo: {
            CONNECTING_STRING: "mongodb://127.0.0.1/roundglass"
        }
    },
    $default: {
        cryptoConfig: {
            ALGORITHM: "aes-256-ctr",
            KEY: "wbedibeiwhduwiehdwuihd;ixhewow;do034i0348-0348-034vyiuGVvbgvgv89873%$%#"
        },
        port:{
            PORT:8080
        },
        jwtConfig: {
            SECRET_KEY: "roundglass_123QWERT_KEY",
            EXPIRE: "10 days"
        },
        mongo: {
            CONNECTING_STRING: "mongodb://127.0.0.1/roundglass"
        }
    }
}

Store.load(APP_CONFIG)

const get = (key, criteria) => {
    if (criteria) {
        return Store.get(key, criteria)
    }
    else {
        return Store.get(key)
    }
}

module.exports = {
    get
}

